#!/bin/bash

ldapsearch -h 10.0.0.1 -p 389 -D uid=admin,cn=users,cn=accounts,dc=wycked,dc=io \
        -b "cn=users,cn=accounts,dc=wycked,dc=io" \
        -b "(&(objectClass=posixgroup)(|(cn=sys_admin)(cn=guacamole)))" \
        -b "cn=groups,cn=accounts,dc=wycked,dc=io" \
        -LLL -vv -W


# ldapsearch -h myServer -p 5201 -D cn=admin,cn=Administrators,cn=config
#  -b "dc=example,dc=com" -s sub "(objectclass=*)"
