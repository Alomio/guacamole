# [Guacamole]
Apache Guacamole is a clientless remote desktop gateway.

## Getting Started 

### Installation
This repository was created to start up Guacamole using [Docker Compose] with LDAP authentication.
Create a `.env` file from `.env.sample` and run `docker-compose up -d`.

### Access
Once the Guacamole image is running, Guacamole will be accessible at http://HOSTNAME:8080/guacamole/, where HOSTNAME is the hostname or address of the
machine hosting Docker.

### Configuration
Any configuration changes can be done in the .env file. Ensure that you copy the `.env.sample` to `.env` to be used with docker-compose.

|Variable | Description | Default Value |
|:---|:---|---:|
|LDAP_HOSTNAME |LDAP server to use | 10.0.0.1 |
|LDAP_PORT |LDAP server port |389 |
|LDAP_USER_BASE_DN | where to find users |cn=users,cn=accounts,dc=wycked,dc=io |
|LDAP_GROUP_BASE_DN | DN for groups |cn=groups,cn=accounts,dc=wycked,dc=io |
|LDAP_GROUP_SEARCH_FILTER | Who should have access to guacamole |(&(objectClass=posixgroup)(|(cn=sys_admin)(cn=guacamole))) |
|LDAP_SEARCH_BIND_DN |who to authenticate as with ldap |uid=admin,cn=users,cn=accounts,dc=wycked,dc=io |
|LDAP_SEARCH_BIND_PASSWORD | password used to authenticate with LDAP | |
|LDAP_USERNAME_ATTRIBUTE |attribute used to identify a person  | uid |
|LDAP_CONFIG_BASE_DN | Not super sure actually, I think it's where it finds groups even though we have that |cn=grouops,cn=accounts,dc=wycked,dc=io |
|POSTGRES_DATABASE | Name of the database to use; Must match POSTGRES_DB |guacamole_db |
|POSTGRES_DB | Name of the database to use; Must match POSTGRES_DATABASE |guacamole_db |
|POSTGRES_PASSWORD | Password for postgres user, will be used by postgres and guacamole internally |YourMadeUpPassword |
|POSTGRES_USER | User for postgres and guacamole to use when running commands | avocado |

You can find more configuration options in the [Guac Docs]
## Administrator
To have an administrator account we added both database authentication and LDAP. One the service comes up, login with `guacadmin/guacadmin`. 
To create another admin, create a new user with the same username as the person you want to be admin from LDAP. They do not need a password but do
need admin privilege. Guacamole will connect the two accounts. Please remember to reset the password for the guacadmin user or deactivate after
creating new admin accounts. 

## Troubleshoot
Ensure the `init/` directory has privileges 777 so it can be executed within the postgres contianer. 
You can use the `test-ldap.sh` script to see if you're able to properly connect with the ldap server.

## Documentation

 - Docker Install instructions: https://guacamole.apache.org/doc/gug/guacamole-docker.html

<!-- Links used above -->
[Guacamole]: https://guacamole.apache.org/
[Docker Compose]: https://docs.docker.com/compose/
[Guac Docs]: https://guacamole.apache.org/doc/gug/configuring-guacamole.html
